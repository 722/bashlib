load ../build/bashlib

@test "can be called with a bash script" {
  PATH="./build:${PATH}"

  run bashlib test/cases/hello-world.sh

  [ "${status}" -eq 0 ]
  echo ${output} | strip_ascii
  [ "${output}" = ":: [hello-world.sh] got here" ]
}

@test "can be called from a shebang" {
  PATH="./build:${PATH}"
  FILE="$(mktemp)"

  run cat test/cases/hello-world.sh > ${FILE} &>/dev/null
  run chmod +x ${FILE} &>/dev/null
  run ${FILE}

  [ "${status}" -eq 0 ]
  [ "$(echo ${lines[0]} | strip_colors)" = ":: [$(basename ${FILE})] got here" ]
}
