if [ -x ${1} ]; then
  exec ${@}
elif [ -f ${1} ]; then
  source ${@}
else
  echo "source $(file_path)"
fi
