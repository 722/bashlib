strip_colors() {
  sed 's/\x1b\[[0-9;]*m//g' $@
}

strip_ascii() {
  sed 's/\x1b\[[0-9;]*[a-zA-Z]//g' $@
}
