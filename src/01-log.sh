color() {
  local color="${1}"

  shift

  local text="${@}"

  which tput &>/dev/null && tput setaf $color

  echo -n $text

  which tput &>/dev/null && tput sgr0

  true
}

log_prefix() {
  echo -n ":: [$(basename ${BASH_SOURCE[2]})]"
}

die() {
  error ${@}
  exit 1
}

error() {
  color 1 "$(log_prefix) ERROR:"
  echo " ${@}"
}

warn() {
  color 3 "$(log_prefix) WARN:"
  echo " ${@}"
}

info() {
  color 2 $(log_prefix)
  echo " ${@}"
}

debug() {
  [[ "${DEBUG}" == "" ]] || (
    color 8 "$(log_prefix) ${@}"
    echo
  )
}

input() {
  color 4 "$(log_prefix) INPUT:"
  echo -n " ${@}"
}
