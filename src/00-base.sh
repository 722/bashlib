file_root() {
  pushd "$(dirname "${BASH_SOURCE[2]}")" >/dev/null 2>&1
  pwd -P
  popd >/dev/null 2>&1
}

file_path() {
  echo "$(file_root)/$(basename ${BASH_SOURCE[1]})"
}

is_linux() {
  [[ "$(uname -s)" == "Linux" ]]
}

is_mac() {
  [[ "$(uname -s)" == "Darwin" ]]
}
