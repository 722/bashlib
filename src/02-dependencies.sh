check_dependency() {
	local program_name=${1}
	local cmd=${2:-$program_name}

	debug "Checking for dependency: ${program_name}"
	which $cmd &>/dev/null
}

depends() {
	check_dependency ${@} || die "$program_name is not installed. Install it and try again."
}

depends_linux() {
	(is_linux && depends ${@}) || debug "Dependency ignored: ${1}"
}

depends_mac() {
	(is_mac && depends ${@}) || debug "Dependency ignored: ${1}"
}

depends_opt() {
	check_dependency ${@} || info "Optional dependency ${1} is ignored." || true
}

depends_mac_opt() {
	is_mac && check_dependency ${@} || info "Optional dependency ${1} is ignored." || true
}
