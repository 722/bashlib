PREFIX ?= /usr/local

.PHONY: format
format:
	git ls-files | grep .sh | xargs shfmt -w

.PHONY: clean
clean:
	@echo "Cleaning up"
	rm -rf build

.PHONY: build
build: clean
	@echo "Building"
	mkdir -p build
	echo "#!/usr/bin/env bash" > build/bashlib
	for file in $$(find src -name "*.sh" | sort -n); do echo "# $${file}" >> build/bashlib; cat $${file} | grep -v -e "^\s*$$" -e "^\s*#[^!]" | grep . >> build/bashlib; echo >> build/bashlib; done
	(which shfmt &>/dev/null) && shfmt -w build/bashlib
	chmod +x build/bashlib
	cp build/bashlib build/bashlib.bash

.PHONY: test
test: build
	@echo "Running all tests"
	bats -p $$(find test -name "*.test.sh")

.PHONY: install
install: build
	@echo "Installing"
	install -D -m 755 ./build/bashlib ${PREFIX}/bin/bashlib
